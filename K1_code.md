Evolutionary and Functional History of the Escherichia coli K1 Capsule
================
Sergio Arredondo-Alonso

-   <a href="#r-libraries" id="toc-r-libraries">R libraries</a>
-   <a href="#metadata-of-the-project"
    id="toc-metadata-of-the-project">Metadata of the project</a>
    -   <a href="#prevalence-of-k1-cps-locus-in-the-distinct-cc-lineages"
        id="toc-prevalence-of-k1-cps-locus-in-the-distinct-cc-lineages">Prevalence
        of K1-cps locus in the distinct CC lineages</a>
-   <a
    href="#horesh-collection-discarding-an-association-between-diarrhoeagenic-isolates-and-the-k1-cps-locus"
    id="toc-horesh-collection-discarding-an-association-between-diarrhoeagenic-isolates-and-the-k1-cps-locus">Horesh
    collection (Discarding an association between diarrhoeagenic isolates
    and the K1-cps locus)</a>
-   <a href="#phylogeny" id="toc-phylogeny">Phylogeny</a>
    -   <a href="#aminoacid-variation-in-the-k1-cps-operon"
        id="toc-aminoacid-variation-in-the-k1-cps-operon">Aminoacid variation in
        the K1-cps operon</a>
-   <a href="#plotting-the-cps-operon"
    id="toc-plotting-the-cps-operon">Plotting the cps operon</a>
-   <a href="#pai-in-cc95" id="toc-pai-in-cc95">PAI in CC95</a>
-   <a href="#dating-the-phylogenies-of-the-main-lineages"
    id="toc-dating-the-phylogenies-of-the-main-lineages">Dating the
    phylogenies of the main lineages</a>
-   <a href="#snp-differences-in-the-region-2-of-the-k1-cps-locus"
    id="toc-snp-differences-in-the-region-2-of-the-k1-cps-locus">SNP
    differences in the region 2 of the K1-cps locus</a>
-   <a href="#r-session" id="toc-r-session">R session</a>

# R libraries

``` r
library(tidyverse)
```

    ## Warning: replacing previous import 'lifecycle::last_warnings' by
    ## 'rlang::last_warnings' when loading 'pillar'

    ## Warning: replacing previous import 'lifecycle::last_warnings' by
    ## 'rlang::last_warnings' when loading 'tibble'

    ## Warning: replacing previous import 'lifecycle::last_warnings' by
    ## 'rlang::last_warnings' when loading 'hms'

    ## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.1 ──

    ## ✔ ggplot2 3.3.6     ✔ purrr   0.3.4
    ## ✔ tibble  3.1.3     ✔ dplyr   1.0.7
    ## ✔ tidyr   1.1.3     ✔ stringr 1.4.0
    ## ✔ readr   1.4.0     ✔ forcats 0.5.1

    ## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
    ## ✖ dplyr::filter() masks stats::filter()
    ## ✖ dplyr::lag()    masks stats::lag()

``` r
library(ggtree)
```

    ## Registered S3 method overwritten by 'treeio':
    ##   method     from
    ##   root.phylo ape

    ## ggtree v3.5.1.902 For help: https://yulab-smu.top/treedata-book/
    ## 
    ## If you use the ggtree package suite in published research, please cite
    ## the appropriate paper(s):
    ## 
    ## Guangchuang Yu, David Smith, Huachen Zhu, Yi Guan, Tommy Tsan-Yuk Lam.
    ## ggtree: an R package for visualization and annotation of phylogenetic
    ## trees with their covariates and other associated data. Methods in
    ## Ecology and Evolution. 2017, 8(1):28-36. doi:10.1111/2041-210X.12628
    ## 
    ## LG Wang, TTY Lam, S Xu, Z Dai, L Zhou, T Feng, P Guo, CW Dunn, BR
    ## Jones, T Bradley, H Zhu, Y Guan, Y Jiang, G Yu. treeio: an R package
    ## for phylogenetic tree input and output with richly annotated and
    ## associated data. Molecular Biology and Evolution. 2020, 37(2):599-603.
    ## doi: 10.1093/molbev/msz240
    ## 
    ## Guangchuang Yu, Tommy Tsan-Yuk Lam, Huachen Zhu, Yi Guan. Two methods
    ## for mapping and visualizing associated data on phylogeny using ggtree.
    ## Molecular Biology and Evolution. 2018, 35(12):3041-3043.
    ## doi:10.1093/molbev/msy194

    ## 
    ## Attaching package: 'ggtree'

    ## The following object is masked from 'package:tidyr':
    ## 
    ##     expand

``` r
library(cowplot)
library(ggfittext)
library(gggenes)
library(RColorBrewer)
library(ape)
```

    ## 
    ## Attaching package: 'ape'

    ## The following object is masked from 'package:ggtree':
    ## 
    ##     rotate

``` r
library(Rtsne)
```

# Metadata of the project

``` r
# This metadata can also be accessed through https://microreact.org/project/cm8f8adPyWoQqemAAgvrmi-k1-context

metadata_project <- read.csv(file = 'metadata/Sergio-et-al-K1-context-metadata.csv') 

metadata_project$CC__autocolour <- as.character(metadata_project$CC__autocolour)

metadata_project$CC__autocolour[which(metadata_project$CC__autocolour == '')] <- 'Other'

metadata_project$CC__autocolour[which(metadata_project$ST__autocolour == 1193)] <- 'CC1193'
```

## Prevalence of K1-cps locus in the distinct CC lineages

``` r
# To estimate the prevalence of K1 in bloodstream infections, we can only consider the NORM and BSAC collections 

norm_bsac <- subset(metadata_project, metadata_project$Collection__autocolour %in% c('NORM Gladstone et al. 2021','BSAC Kallonen et al. 2017','CUH Kallonen et al. 2017'))

# Let's merge together the BSAC and CUH collections 

norm_bsac$Collection <- as.character(norm_bsac$Collection__autocolour)
norm_bsac$Collection[which(norm_bsac$Collection == 'CUH Kallonen et al. 2017')] <- 'BSAC Kallonen et al. 2017'

# K1-cps locus per phylogroup

norm_bsac %>%
  group_by(Collection, Phylotype_phylogenetically_corrected__autocolour) %>%
  filter(Gene_count__autocolour == 14) %>%
  count(sort = TRUE)
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Collection"],"name":[1],"type":["chr"],"align":["left"]},{"label":["Phylotype_phylogenetically_corrected__autocolour"],"name":[2],"type":["fct"],"align":["left"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"NORM Gladstone et al. 2021","2":"B2","3":"693"},{"1":"BSAC Kallonen et al. 2017","2":"B2","3":"300"},{"1":"NORM Gladstone et al. 2021","2":"F","3":"68"},{"1":"BSAC Kallonen et al. 2017","2":"F","3":"35"},{"1":"NORM Gladstone et al. 2021","2":"A","3":"16"},{"1":"BSAC Kallonen et al. 2017","2":"A","3":"8"},{"1":"NORM Gladstone et al. 2021","2":"D","3":"3"},{"1":"BSAC Kallonen et al. 2017","2":"D","3":"2"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
# K1-cps locus per CC, this numbers are given in Table 1 

k1_perCC <- norm_bsac %>%
  group_by(Collection, CC__autocolour) %>%
  filter(Gene_count__autocolour == 14) %>%
  count(sort = TRUE)


# Let's compute the frequencies 

prevalence_CC <- norm_bsac %>%
  group_by(Collection, CC__autocolour) %>%
  count()

prevalence_k1 <- norm_bsac %>%
  group_by(Collection, CC__autocolour) %>%
  filter(Gene_count__autocolour == 14) %>%
  count()

# Statistical association between K1-cps locus and CC 


for(clonal_complex in c('CC95','CC141','CC144','CC59','CC62','CC80','CC10','CC131','CC1193'))
{
  
  # BSAC
  
  total_bsac <- subset(prevalence_CC$n, prevalence_CC$Collection == 'BSAC Kallonen et al. 2017' & prevalence_CC$CC__autocolour == clonal_complex)
  total_k1 <- subset(prevalence_k1$n, prevalence_k1$Collection == 'BSAC Kallonen et al. 2017' & prevalence_k1$CC__autocolour == clonal_complex)
  
  other_bsac <- sum(subset(prevalence_CC$n, prevalence_CC$Collection == 'BSAC Kallonen et al. 2017' & prevalence_CC$CC__autocolour != clonal_complex))
  other_k1 <- sum(subset(prevalence_k1$n, prevalence_k1$Collection == 'BSAC Kallonen et al. 2017' & prevalence_k1$CC__autocolour != clonal_complex))
  
  print(clonal_complex)
  print('BSAC')
  
  M <- as.table(rbind(c(total_k1, other_k1), c(total_bsac, other_bsac)))
  dimnames(M) <- list(K1 = c('K1+', 'K1-'),
                    Complex = c('ST','Other_ST'))
  Xsq <- chisq.test(M)
  print(Xsq$p.value)/length(16)
  
  # NORM
  
  total_norm <- subset(prevalence_CC$n, prevalence_CC$Collection == 'NORM Gladstone et al. 2021' & prevalence_CC$CC__autocolour == clonal_complex)
  total_k1 <- subset(prevalence_k1$n, prevalence_k1$Collection == 'NORM Gladstone et al. 2021' & prevalence_k1$CC__autocolour == clonal_complex)
  
  other_norm <- sum(subset(prevalence_CC$n, prevalence_CC$Collection == 'NORM Gladstone et al. 2021' & prevalence_CC$CC__autocolour != clonal_complex))
  other_k1 <- sum(subset(prevalence_k1$n, prevalence_k1$Collection == 'NORM Gladstone et al. 2021' & prevalence_k1$CC__autocolour != clonal_complex))
  
  print(clonal_complex)
  print('NORM')
  
  M <- as.table(rbind(c(total_k1, other_k1), c(total_norm, other_norm)))
  dimnames(M) <- list(K1 = c('K1+', 'K1-'),
                    Complex = c('CC','Other_CC'))
  Xsq <- chisq.test(M)
  print(Xsq$p.value)/length(16)

}
```

    ## [1] "CC95"
    ## [1] "BSAC"
    ## [1] 1.378501e-68
    ## [1] "CC95"
    ## [1] "NORM"
    ## [1] 3.873956e-149
    ## [1] "CC141"
    ## [1] "BSAC"
    ## [1] 2.572503e-06
    ## [1] "CC141"
    ## [1] "NORM"
    ## [1] 1.461879e-19
    ## [1] "CC144"
    ## [1] "BSAC"
    ## [1] 8.888486e-08
    ## [1] "CC144"
    ## [1] "NORM"
    ## [1] 2.277899e-11
    ## [1] "CC59"
    ## [1] "BSAC"
    ## [1] 0.0003852522
    ## [1] "CC59"
    ## [1] "NORM"
    ## [1] 1.692016e-11
    ## [1] "CC62"
    ## [1] "BSAC"
    ## [1] 2.934986e-06
    ## [1] "CC62"
    ## [1] "NORM"
    ## [1] 4.170274e-06
    ## [1] "CC80"
    ## [1] "BSAC"
    ## [1] 1.797689e-07
    ## [1] "CC80"
    ## [1] "NORM"
    ## [1] 3.345941e-12
    ## [1] "CC10"
    ## [1] "BSAC"
    ## [1] 0.6361949
    ## [1] "CC10"
    ## [1] "NORM"
    ## [1] 0.4246078
    ## [1] "CC131"
    ## [1] "BSAC"
    ## [1] 4.131231e-12
    ## [1] "CC131"
    ## [1] "NORM"
    ## [1] 1.372058e-10
    ## [1] "CC1193"
    ## [1] "BSAC"
    ## [1] 7.761736e-195
    ## [1] "CC1193"
    ## [1] "NORM"
    ## [1] 1.314265e-05

# Horesh collection (Discarding an association between diarrhoeagenic isolates and the K1-cps locus)

``` r
horesh_metadata <- read.csv(file = 'metadata/F1_genome_metadata_K1_pathovars.csv')


horesh_metadata$Group <- ifelse(horesh_metadata$Pathotype == 'COMMENSAL','COMMENSAL',
                        ifelse(horesh_metadata$Pathotype %in% c('ExPEC (predicted)','ExPEC'), 'Extraintestinal (ExPEC)',
                        ifelse(horesh_metadata$Pathotype == 'Not determined','Not determined','Diarrheagenic')))

horesh_diarrhoeagenic <- subset(horesh_metadata,! horesh_metadata$Pathotype %in% c('COMMENSAL','Not determined','ExPEC (predicted)','ExPEC'))

horesh_diarrhoeagenic %>%
  group_by(Group, K1) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Group"],"name":[1],"type":["chr"],"align":["left"]},{"label":["K1"],"name":[2],"type":["int"],"align":["right"]},{"label":["n"],"name":[3],"type":["int"],"align":["right"]}],"data":[{"1":"Diarrheagenic","2":"0","3":"5231"},{"1":"Diarrheagenic","2":"1","3":"5"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

# Phylogeny

``` r
phylogenetic_tree <- read.tree(file = 'supertree/BSAC-NORM-Nicks-MGEN-Escherichia-coli-ST131-strain-EC958-v1-concatenated-SNPs-ATGC.nwk')

phylogenetic_tree_metadata <- data.frame(ID = phylogenetic_tree$tip.label)

tree <- ggtree(phylogenetic_tree) + geom_treescale(x=0.3,y=75) 

metadata_project$K1 <- ifelse(metadata_project$Gene_count__autocolour >= 14, 'K1_Positive',NA)

tree_k1 <- tree  %<+% metadata_project + geom_tippoint(aes(color=K1), size = 0.75) + scale_color_manual(values = c('coral2','gray2')) + theme(legend.position="bottom") 

meta_tree <- merge(phylogenetic_tree_metadata, metadata_project, by = 'ID')

meta_tree$Phylogroup <- meta_tree$Phylotype_phylogenetically_corrected__autocolour

tree_k1 + geom_facet(panel = "Phylogroup",  data = meta_tree, geom = geom_tile, mapping=aes(x = 1, fill= Phylogroup))+ scale_fill_brewer(palette = 'Paired') + theme(legend.position="none") 
```

![](K1_code_files/figure-gfm/unnamed-chunk-5-1.png)<!-- -->

``` r
meta_tree$Position <- ifelse(meta_tree$CC__autocolour == 'CC95',5,
                                     ifelse(meta_tree$CC__autocolour == 'CC141',7,
                                     ifelse(meta_tree$CC__autocolour == 'CC59',3,
                                     ifelse(meta_tree$CC__autocolour == 'CC144',8,
                                     ifelse(meta_tree$CC__autocolour == 'CC80',4,
                                     ifelse(meta_tree$CC__autocolour == 'CC62',2,
                                     ifelse(meta_tree$CC__autocolour == 'CC10',1,
                                     ifelse(meta_tree$CC__autocolour == 'CC131',6,
                                     ifelse(meta_tree$CC__autocolour == 'CC1193', 9,10)))))))))


meta_tree$Phylogroup_Position <- ifelse(meta_tree$Phylogroup == 'A',4,
                                     ifelse(meta_tree$Phylogroup == 'B1',6,
                                     ifelse(meta_tree$Phylogroup == 'B2',8,
                                     ifelse(meta_tree$Phylogroup == 'C',5,
                                     ifelse(meta_tree$Phylogroup == 'D',7,
                                     ifelse(meta_tree$Phylogroup == 'F',2,
                                     ifelse(meta_tree$Phylogroup == 'G',1,
                                     ifelse(meta_tree$Phylogroup == 'E',3,100))))))))

phylo_k1 <- tree_k1 + geom_facet(panel = "PG",  data = meta_tree, geom = geom_tile, mapping=aes(x = 1, fill = Phylogroup), alpha = 0.5) + theme(legend.position="none") 

facet_widths(phylo_k1, widths = c(4, 2))
```

![](K1_code_files/figure-gfm/unnamed-chunk-5-2.png)<!-- -->

``` r
only_k1 <- subset(meta_tree, meta_tree$K1 == 'K1_Positive')


# Defining colours for plotting reasons

n <- 20

qual_col_pals <- brewer.pal.info[brewer.pal.info$category == 'qual',]

col_vector <- unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

col_vector[1] <- '#86fdfd'

final_colurs <- c(col_vector[1:4],col_vector[18],col_vector[5:17])

only_k1$CC <- only_k1$CC__autocolour

phylo_k1_cc <- phylo_k1 + geom_facet(panel = "CC",  data = only_k1, geom = geom_tile, mapping=aes(x = 1, fill = CC)) + scale_fill_manual(values = final_colurs)  + theme(legend.position="right") 

facet_widths(phylo_k1_cc, widths = c(4, 0.5, 0.5))
```

![](K1_code_files/figure-gfm/unnamed-chunk-5-3.png)<!-- -->

## Aminoacid variation in the K1-cps operon

``` r
aa_allele_meta <- meta_tree[,grep(pattern = 'AA__autocolour', x = colnames(meta_tree))]

aa_allele_meta <- apply(aa_allele_meta, 2, as.character)

rownames(aa_allele_meta) <- meta_tree$ID

colnames(aa_allele_meta) <- gsub(pattern = '_AA', replacement = '', x = colnames(aa_allele_meta))

aa_allele_meta <- aa_allele_meta[,c(14,13,12,11,10,9,8,7,6,5,4,3,2,1)]

colnames(aa_allele_meta) <- gsub(pattern = '_AA__autocolour', replacement = '', x = colnames(aa_allele_meta))

aa_allele_meta[is.na(aa_allele_meta)] <- 0

# Colours used for the plots 

palette_colours <- c("dodgerblue2","#E31A1C", "#FDBF6F","green4","#6A3D9A", "#FF7F00", "gold1", "antiquewhite","palegreen2", "maroon", "skyblue2", "gray70","darkturquoise", "darkorange4", "black")
n <- 105

qual_col_pals <- brewer.pal.info[brewer.pal.info$category == 'qual',]

col_vector <- unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))

col_vector <- c('white',palette_colours, col_vector, palette_colours, col_vector)

gheatmap(tree_k1, aa_allele_meta, offset=0.01, width=1.0, color=NULL, colnames=FALSE) + scale_fill_manual(values=col_vector) + theme(legend.position = 'none')
```

    ## Warning: attributes are not identical across measure variables;
    ## they will be dropped

    ## Scale for 'y' is already present. Adding another scale for 'y', which will
    ## replace the existing scale.

    ## Scale for 'fill' is already present. Adding another scale for 'fill', which
    ## will replace the existing scale.

![](K1_code_files/figure-gfm/unnamed-chunk-6-1.png)<!-- -->

# Plotting the cps operon

``` r
k1_operon <- read.csv(file = 'metadata/k1_operon.csv')

ggplot(k1_operon, aes(xmin = start, xmax = end, y = capsule, fill = region, label = gene, forward = orientation)) + 
  facet_wrap(~ capsule, scales = "free", ncol = 1) +
  geom_gene_arrow(    arrowhead_height = grid::unit(12, "mm"),
  arrowhead_width = grid::unit(6, "mm"),
  arrow_body_height = grid::unit(6, "mm")) +
  geom_gene_label() +
  scale_fill_brewer(palette = "Set3") +
  theme_genes() %+replace% theme(text = element_text(size=16),
                                   axis.title.x=element_text(),
                                   axis.ticks.y = element_blank(),
                                   axis.text.y=element_blank(), 
                                   panel.grid.major = element_blank(), 
                                   panel.grid.minor = element_blank(),
                                   legend.direction="horizontal",
                                   legend.position="bottom",
                                   plot.margin=grid::unit(c(0,0,0,0), "mm")) 
```

![](K1_code_files/figure-gfm/unnamed-chunk-7-1.png)<!-- -->

# PAI in CC95

``` r
# Which CC95 carry or not the PAI?

abricate_k1 <- read.csv(file = 'abricate/abricate_k1.csv')
abricate_k1$ID <- gsub(pattern = '.contigs_velvet.fa', replacement = '', x = abricate_k1$X.FILE)

kpsF_coordinates <- subset(abricate_k1, abricate_k1$GENE == 'kpsF_1')

abricate_pheV <- read.csv(file = 'abricate/abricate_pheV.csv')

abricate_integrase <- read.csv(file = 'abricate/abricate_integrase.csv')
abricate_integrase <- subset(abricate_integrase, abricate_integrase$X.IDENTITY > 95)

kpsF_nexto_pheV <- NULL

integrase_nexto_pheV <- NULL

single_pai_contig <- NULL


for(kpsF in kpsF_coordinates$X.FILE)
{
  pheV_genome <- subset(abricate_pheV, abricate_pheV$X.FILE == kpsF)
  kpsF_genome <- subset(kpsF_coordinates, kpsF_coordinates$X.FILE == kpsF)
  
  pheV_kpsF_contig <- subset(pheV_genome, pheV_genome$SEQUENCE %in% kpsF_genome$SEQUENCE)
  pheV_kpsF_contig$Genome_Distance <- abs(pheV_kpsF_contig$START - kpsF_genome$START)
  
  kpsF_nexto_pheV <- rbind(kpsF_nexto_pheV, pheV_kpsF_contig)
  
  integrase_genome <- subset(abricate_integrase, abricate_integrase$X.FILE == kpsF)
  
  pheV_integrase_contig <- subset(pheV_genome, pheV_genome$SEQUENCE %in% integrase_genome$SEQUENCE)
  
  integrase_contig <- subset(integrase_genome, integrase_genome$SEQUENCE %in% pheV_integrase_contig$SEQUENCE)
  
  integrase_contig <- integrase_contig[order(-integrase_contig$X.COVERAGE),]
  
  pheV_integrase_contig$Genome_Distance <- abs(pheV_integrase_contig$START - integrase_contig$START[1])
  
  integrase_nexto_pheV <- rbind(integrase_nexto_pheV, pheV_integrase_contig)
  
  if(as.character(kpsF_genome$SEQUENCE[1]) == as.character(pheV_kpsF_contig$SEQUENCE[1]))
  {
    if(nrow(integrase_contig) > 0)
    {
      if(nrow(pheV_kpsF_contig) == 2)
      {
        if(as.character(integrase_contig$SEQUENCE[1]) == as.character(pheV_kpsF_contig$SEQUENCE[1]))
        {
          if(as.character(pheV_integrase_contig$SEQUENCE[1]) == as.character(pheV_kpsF_contig$SEQUENCE[1]))
          {
            difference <- abs(pheV_integrase_contig$START[2] - pheV_integrase_contig$START[1])
            estimated_pai_size <- data.frame(X.FILE = kpsF,
                       PAI_Size = difference)
            
            single_pai_contig <- rbind(single_pai_contig, estimated_pai_size)
            
          }
        }
      }
    }
  }
}


kpsF_nexto_pheV <- subset(kpsF_nexto_pheV, kpsF_nexto_pheV$Genome_Distance < 5e3)

no_pai_genomes <- subset(kpsF_nexto_pheV, kpsF_nexto_pheV$X.COVERAGE >= 90)

possible_pai_genomes <- subset(kpsF_nexto_pheV, kpsF_nexto_pheV$X.COVERAGE < 90)

integrase_nexto_pheV <- subset(integrase_nexto_pheV,! integrase_nexto_pheV$X.FILE %in% no_pai_genomes$X.FILE)

integrase_nexto_pheV <- subset(integrase_nexto_pheV, integrase_nexto_pheV$Genome_Distance < 5e3)

integrase_nexto_pheV <- integrase_nexto_pheV[! duplicated(integrase_nexto_pheV$X.FILE),]

meta_no_pai <- subset(no_pai_genomes, select = c('X.FILE'))
meta_pai <- subset(integrase_nexto_pheV, select = c('X.FILE'))


meta_no_pai$ID <- gsub(pattern = '.contigs_velvet.fa', replacement = '', x = meta_no_pai$X.FILE)
meta_pai$ID <- gsub(pattern = '.contigs_velvet.fa', replacement = '', x = meta_pai$X.FILE)

meta_unknown <- data.frame(X.FILE =  setdiff(possible_pai_genomes$X.FILE, integrase_nexto_pheV$X.FILE))
meta_unknown$ID <- gsub(pattern = '.contigs_velvet.fa', replacement = '', x = meta_unknown$X.FILE)

meta_pai$Structure <- 'PAIV'
meta_no_pai$Structure <- 'NO-PAIV'
meta_unknown$Structure <- 'Unknown'

all_meta <- rbind(meta_no_pai, meta_pai)
all_meta <- rbind(all_meta, meta_unknown)

all_meta %>%
  group_by(Structure) %>%
  count()
```

<div data-pagedtable="false">

<script data-pagedtable-source type="application/json">
{"columns":[{"label":["Structure"],"name":[1],"type":["chr"],"align":["left"]},{"label":["n"],"name":[2],"type":["int"],"align":["right"]}],"data":[{"1":"NO-PAIV","2":"70"},{"1":"PAIV","2":"354"},{"1":"Unknown","2":"17"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>

</div>

``` r
bd_st95 <- read.tree(file = 'dating_analysis/Escherichia_coli_ST95_32_GCF_008632595_noref_Noplasmid_100000000_1000_arc_BD.tre')

bd_st95_tree <- ggtree(bd_st95 ,mrsd='2017-01-01') + theme_tree2() + scale_x_continuous(labels = abs, limits = c(1750, 2018), breaks = c(1768.0984, 1823.8012, 1840.1355, 1864.4729, 1904.7998))

bd_st95_tree_metadata <- data.frame(ID = bd_st95$tip.label)

meta_tree <- merge(bd_st95_tree_metadata, all_meta, by = 'ID')

#meta_tree <- merge(meta_tree, all_meta, by = 'ID')

pai_metadata <- subset(meta_tree, select = c('ID','Structure'))


meta_tree <- merge(bd_st95_tree_metadata, metadata_project, by = 'ID')

meta_tree$K1_Presence_Absence <- ifelse(meta_tree$Gene_count__autocolour >= 14, 1, 2)
meta_tree$K1_Text <- ifelse(meta_tree$Gene_count__autocolour >= 14, 'Present', 'Absent')

meta_tree$K1_Text[is.na(meta_tree$K1_Text)] <- 'Absent'

final_bd_st95_tree <- ggtree(bd_st95 ,mrsd='2017-01-01') %<+% meta_tree + geom_tippoint(aes(color=K1_Text), size = 2.0) + scale_color_manual(values = c('gray80','coral2')) + geom_facet(panel = "PAI",  data = pai_metadata, geom = geom_tile, mapping=aes(x = 1, fill = Structure)) + scale_fill_manual(values=c('purple','forestgreen','orange')) + theme_tree2() + labs(color = 'K1-cps present', fill = 'PAI') + geom_tiplab(size = 0.5)

final_bd_st95_tree <- ggtree(bd_st95 ,mrsd='2017-01-01') + geom_facet(panel = "K1",  data = meta_tree, geom = geom_tile, mapping=aes(x = 1, fill = K1_Text)) + geom_facet(panel = "PAI",  data = pai_metadata, geom = geom_tile, mapping=aes(x = 1, fill = Structure)) + scale_fill_manual(values=c('gray80','purple','forestgreen','coral2','orange')) + theme_tree2()


facet_widths(final_bd_st95_tree, widths = c(4, 0.2, 0.2))
```

![](K1_code_files/figure-gfm/unnamed-chunk-8-1.png)<!-- -->

# Dating the phylogenies of the main lineages

``` r
## CC95

bd_cc95 <- read.tree(file = 'dating_analysis/Escherichia_coli_ST95_32_GCF_008632595_noref_Noplasmid_100000000_1000_arc_BD.tre')


bd_cc95_tree <- ggtree(bd_cc95 ,mrsd='2017-01-01') + theme_tree2() + scale_x_continuous(labels = abs, limits = c(1500, 2018), breaks = c(1768))

bd_cc95_tree_metadata <- data.frame(ID = bd_cc95$tip.label)

meta_tree <- merge(bd_cc95_tree_metadata, metadata_project, by = 'ID')

meta_tree$K1_Presence_Absence <- ifelse(meta_tree$Gene_count__autocolour >= 14, 1, 2)
meta_tree$K1_Text <- ifelse(meta_tree$Gene_count__autocolour >= 14, 'Present', 'Absent')

meta_tree$K1_Text[is.na(meta_tree$K1_Text)] <- 'Absent'

dated_k1_cc95 <- bd_cc95_tree  %<+% meta_tree + geom_tippoint(aes(color=K1_Text), size = 2.0) + scale_color_manual(values = c('gray80','coral2')) + theme_classic(base_size = 16) + theme(legend.position='none',  axis.text.y=element_blank(), axis.ticks.y=element_blank(), axis.line.y = element_blank(), axis.text.x=element_blank()) + ggtitle("CC95")

## CC59

bd_cc59 <- read.tree(file = 'dating_analysis/CC59_Escherichia_coli_AR_0013_GCF_003571685_1_concatenated_noref_noplasmid_100000000_1000_arc_BD.tre')

bd_cc59_tree <- ggtree(bd_cc59 ,mrsd='2017-01-01') + theme_tree2() + scale_x_continuous(labels = abs, limits = c(1500, 2018), breaks = c(1525))

bd_cc59_tree_metadata <- data.frame(ID = bd_cc59$tip.label)

meta_tree <- merge(bd_cc59_tree_metadata, metadata_project, by = 'ID')

meta_tree$K1_Presence_Absence <- ifelse(meta_tree$Gene_count__autocolour >= 14, 1, 2)
meta_tree$K1_Text <- ifelse(meta_tree$Gene_count__autocolour >= 14, 'Present', 'Absent')

meta_tree$K1_Text[is.na(meta_tree$K1_Text)] <- 'Absent'

dated_k1_cc59 <- bd_cc59_tree  %<+% meta_tree + geom_tippoint(aes(color=K1_Text), size = 2.0) + scale_color_manual(values = c('gray80','coral2')) + theme_classic(base_size = 16) + theme(legend.position='none',  axis.text.y=element_blank(), axis.ticks.y=element_blank(), axis.line.y = element_blank(), axis.text.x=element_blank()) + ggtitle("CC59")


## CC10


bd_cc10 <- read.tree(file = 'dating_analysis/Escherichia_coli_str_K-12_substr_MG1655_v3_concatenated_100000000_1000_arc_BD.tre')

bd_cc10_tree <- ggtree(bd_cc10 ,mrsd='2017-01-01') + theme_tree2() + scale_x_continuous(labels = abs, limits = c(1500, 2018), breaks = c(1789, 1821, 1853, 1873))

bd_cc10_tree_metadata <- data.frame(ID = bd_cc10$tip.label)

meta_tree <- merge(bd_cc10_tree_metadata, metadata_project, by = 'ID')

meta_tree$K1_Presence_Absence <- ifelse(meta_tree$Gene_count__autocolour >= 14, 1, 2)
meta_tree$K1_Text <- ifelse(meta_tree$Gene_count__autocolour >= 14, 'Present', 'Absent')

meta_tree$K1_Text[is.na(meta_tree$K1_Text)] <- 'Absent'

dated_k1_cc10 <- bd_cc10_tree  %<+% meta_tree + geom_tippoint(aes(color=K1_Text), size = 2.0) + scale_color_manual(values = c('gray80','coral2')) + theme_classic(base_size = 16) + theme(legend.position='none',  axis.text.y=element_blank(), axis.ticks.y=element_blank(), axis.line.y = element_blank(), axis.text.x=element_blank()) + ggtitle("CC10")

## CC131


bd_cc131 <- read.tree(file = 'dating_analysis/Ecoli_ST131_EC958_masked_plamsids_MGEs_hotspots_100000000_1000_arc_BD.tre')

bd_cc131_tree <- ggtree(bd_cc131 ,mrsd='2017-01-01') + theme_tree2() + scale_x_continuous(labels = abs, limits = c(1500, 2018), breaks = c(1525, 1768, 1789, 1821, 1853, 1873, 1957, 1999))

bd_cc131_tree_metadata <- data.frame(ID = bd_cc131$tip.label)

meta_tree <- merge(bd_cc131_tree_metadata, metadata_project, by = 'ID')

meta_tree$K1_Presence_Absence <- ifelse(meta_tree$Gene_count__autocolour >= 14, 1, 2)
meta_tree$K1_Text <- ifelse(meta_tree$Gene_count__autocolour >= 14, 'Present', 'Absent')

meta_tree$K1_Text[is.na(meta_tree$K1_Text)] <- 'Absent'

dated_k1_cc131 <- bd_cc131_tree  %<+% meta_tree + geom_tippoint(aes(color=K1_Text), size = 2.0) + scale_color_manual(values = c('gray80','coral2')) + theme_classic(base_size = 16) + theme(legend.position='none',  axis.text.y=element_blank(), axis.ticks.y=element_blank(), axis.line.y = element_blank()) + ggtitle("CC131")



## All phylogenies together

plot_grid(dated_k1_cc59, dated_k1_cc95, dated_k1_cc10, dated_k1_cc131, nrow = 4)
```

![](K1_code_files/figure-gfm/unnamed-chunk-9-1.png)<!-- -->

# SNP differences in the region 2 of the K1-cps locus

``` r
snp_distance_matrix <- read.csv(file = 'snps/matrix.csv', header = FALSE)

rownames(snp_distance_matrix) <- snp_distance_matrix$V1

id_k1_isolates <- snp_distance_matrix$V1

snp_distance_matrix$V1 <- NULL

colnames(snp_distance_matrix) <- id_k1_isolates

snp_gather_distance <- snp_distance_matrix %>%
                      gather(key = Isolate, V1)

snp_gather_distance$Other_isolate <- id_k1_isolates


# Let's look only into NORM genomes


norm_metadata <- subset(metadata_project, select = c('ID','CC__autocolour','Collection__autocolour'))
norm_metadata <- subset(norm_metadata, norm_metadata$Collection__autocolour == 'NORM Gladstone et al. 2021')

comparison_metadata <- subset(norm_metadata, norm_metadata$CC__autocolour %in% c('CC95','CC141','CC144','CC59','CC62','CC80','CC1193','CC131','CC10'))

comparison_snp <- subset(snp_gather_distance, snp_gather_distance$Isolate %in% comparison_metadata$ID & snp_gather_distance$Other_isolate %in% comparison_metadata$ID)

colnames(comparison_snp)[1] <- 'ID'

info_comparison_snp <- merge(comparison_snp, comparison_metadata, by = 'ID')

colnames(info_comparison_snp)[1] <- 'Isolate'
colnames(info_comparison_snp)[3] <- 'ID'

info_comparison_snp <- merge(info_comparison_snp, comparison_metadata, by = 'ID')

info_comparison_snp$Group <- paste(info_comparison_snp$CC__autocolour.x, info_comparison_snp$CC__autocolour.y, sep = '-')

stats_comparison <- info_comparison_snp %>%
  group_by(Group) %>%
  summarise(mean(V1), median(V1))


stats_comparison$CC_First <- str_split_fixed(string = stats_comparison$Group, pattern = '-', n = 2)[,1]
stats_comparison$CC_Second <- str_split_fixed(string = stats_comparison$Group, pattern = '-', n = 2)[,2]

colnames(stats_comparison)[c(2,3)] <- c('Mean_SNP','Median_SNP')

ggplot(stats_comparison, aes(x = as.factor(CC_First), y = as.factor(CC_Second), color = Median_SNP)) + geom_point(size = 10.0 ,alpha = 0.4) + geom_text(aes(label = Median_SNP), size = 5.0) + scale_colour_gradient(low = "red", high = "blue") + theme_bw(base_size = 16) + labs(x = '', y = '', color = 'SNP differences')
```

![](K1_code_files/figure-gfm/unnamed-chunk-10-1.png)<!-- -->

``` r
# Can we draw a phylogeny out of it as well?

## iqtree code: iqtree -s K1_neu_genes_concat_SNPs.aln -pre K1_neu_genes_concat_SNPs -nt 32 -fast -m GTR ##

iqtree_K1_locus <- read.tree(file = 'snps/K1_neu_genes_concat_SNPs.treefile')

# Let's look only into NORM genomes

keep_genomes <- gsub(pattern = '#', replacement = '_', x = norm_metadata$ID)

discard_tips <- drop.tip(iqtree_K1_locus, as.character(keep_genomes))

norm_iq_tree_K1_locus <- drop.tip(iqtree_K1_locus, discard_tips$tip.label)

ggtree(norm_iq_tree_K1_locus) + geom_nodelab() + theme_tree2() # We can see that the resolution is quite poor 
```

![](K1_code_files/figure-gfm/unnamed-chunk-10-2.png)<!-- -->

``` r
# Can we draw a clustering out of it?

comparison_snps_wide <- comparison_snp %>%
  pivot_wider(names_from = Other_isolate, values_from = V1)

rownames(comparison_snps_wide) <- comparison_snps_wide$ID
```

    ## Warning: Setting row names on a tibble is deprecated.

``` r
id_k1_isolates <- comparison_snps_wide$ID

comparison_snps_wide$ID <- NULL

comparison_snps_wide <- as.matrix(comparison_snps_wide)

set.seed(14042023)

snps_tsne <- Rtsne(X = comparison_snps_wide, perplexity = 50, theta = 0.5, is_distance = TRUE)

tsne_df_snps_k1 <- as.data.frame(snps_tsne$Y[,c(1,2)])

tsne_df_snps_k1$ID <- id_k1_isolates

tsne_meta_snps_k1 <- merge(tsne_df_snps_k1, comparison_metadata, by = 'ID')

colours_based_on_tree <- c(final_colurs[5], final_colurs[6],final_colurs[7],final_colurs[8],final_colurs[9],final_colurs[10],final_colurs[11],final_colurs[12], final_colurs[13])

ggplot(tsne_meta_snps_k1, aes(x = V1, y = V2, fill = CC__autocolour, label = CC__autocolour)) + geom_jitter(width = 0.2, height = 0.2, size = 5, alpha = 0.9, shape = 21, color = 'black') + theme_bw() + scale_fill_manual(values = colours_based_on_tree) + labs(x = 'tsne 1D - based on SNP distances K1-cps region 2', y = 'tsne 2D - based on SNP distances K1-cps region 2', fill = 'CC') + theme(text = element_text(size=16))
```

![](K1_code_files/figure-gfm/unnamed-chunk-10-3.png)<!-- -->

``` r
ggplot(tsne_meta_snps_k1, aes(x = V1, y = V2, fill = CC__autocolour, label = CC__autocolour)) + geom_jitter(width = 0.2, height = 0.2, size = 5, alpha = 0.9, shape = 21, color = 'black') + theme_bw() + scale_fill_brewer(palette = 'Paired') + labs(x = 'tsne 1D - based on SNP distances K1-cps region 2', y = 'tsne 2D - based on SNP distances K1-cps region 2', fill = 'CC') + theme(text = element_text(size=16))
```

![](K1_code_files/figure-gfm/unnamed-chunk-10-4.png)<!-- -->

# R session

``` r
sessionInfo()
```

    ## R version 3.6.3 (2020-02-29)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 20.04.6 LTS
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_GB.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_GB.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_GB.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_GB.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] Rtsne_0.15         ape_5.5            RColorBrewer_1.1-2 gggenes_0.4.1     
    ##  [5] ggfittext_0.9.1    cowplot_1.1.1      ggtree_3.5.1.902   forcats_0.5.1     
    ##  [9] stringr_1.4.0      dplyr_1.0.7        purrr_0.3.4        readr_1.4.0       
    ## [13] tidyr_1.1.3        tibble_3.1.3       ggplot2_3.3.6      tidyverse_1.3.1   
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] Rcpp_1.0.6         lubridate_1.8.0    lattice_0.20-40    assertthat_0.2.1  
    ##  [5] digest_0.6.27      utf8_1.1.4         R6_2.5.0           cellranger_1.1.0  
    ##  [9] backports_1.2.1    reprex_2.0.0       evaluate_0.14      httr_1.4.2        
    ## [13] pillar_1.6.2       ggfun_0.0.6        yulab.utils_0.0.5  rlang_1.1.0       
    ## [17] lazyeval_0.2.2     readxl_1.3.1       rstudioapi_0.13    rmarkdown_2.14    
    ## [21] labeling_0.4.2     munsell_0.5.0      broom_0.7.9        compiler_3.6.3    
    ## [25] modelr_0.1.8       xfun_0.31          pkgconfig_2.0.3    gridGraphics_0.5-1
    ## [29] htmltools_0.5.1.1  tidyselect_1.1.0   fansi_0.4.2        crayon_1.4.1      
    ## [33] dbplyr_2.1.1       withr_2.4.1        grid_3.6.3         nlme_3.1-144      
    ## [37] jsonlite_1.7.2     gtable_0.3.0       lifecycle_1.0.0    DBI_1.1.1         
    ## [41] magrittr_2.0.1     scales_1.1.1       tidytree_0.3.9     cli_3.6.0         
    ## [45] stringi_1.5.3      farver_2.0.3       fs_1.5.0           xml2_1.3.2        
    ## [49] ellipsis_0.3.2     generics_0.1.0     vctrs_0.3.8        treeio_1.10.0     
    ## [53] tools_3.6.3        ggplotify_0.1.0    glue_1.4.2         hms_1.0.0         
    ## [57] parallel_3.6.3     yaml_2.2.1         colorspace_2.0-0   aplot_0.1.6       
    ## [61] rvest_1.0.1        knitr_1.30         haven_2.3.1        patchwork_1.1.1
