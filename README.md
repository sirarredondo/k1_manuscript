Code availability
============

This repository contains all the files to run the [Rmd file](K1_code.Rmd) with Bash and R code required to reproduce the results and figures present in the K1 manuscript. You can check the [md version](K1_code.md) which includes a table of contents and the figures after each chunk of code. 

Questions
============

**If there is anything else that you would like to ask, feel free to contact me at s.a.alonso[at].medisin.uio.no**
